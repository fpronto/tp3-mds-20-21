localStorage.clear();

const startGame = document.getElementById("startgame");
if (startGame) {
  startGame.addEventListener("click", () => {
    const overlay = document.getElementsByClassName("initial-overlay-wrapper");
    overlay[0].classList.toggle("show");

    // Score management
    score.startCounting();
  });
}
