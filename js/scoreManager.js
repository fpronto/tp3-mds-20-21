

var score = {
    steps: 0,
    wrongAnswers: 0,
    fullTime: 0,
    startTime: null,

    closedCards: [],

    closeCard: function (cardId) {
        this.closedCards.push(cardId);
    },

    startCounting: function () {
        this.startTime = new Date().getTime();
    },

    finishCounting: function () {
        this.fullTime = new Date().getTime() - this.startTime;
    },

    incrementStep: function () {
        this.steps++;
    },

    addWrongs: function (amount) {
        this.wrongAnswers += amount;
    },

    gameComplete: function () {
        return this.closedCards.length === 5;
    }
}



