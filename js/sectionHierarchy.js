

const cardsHierarchy = {
    seccao1: {
        enablingCards: ['seccao2', 'seccao3']
    },
    seccao2: {
        enablingCards: ['seccao3']
    },
    seccao3: {
        enablingCards: ['seccao4', 'seccao5']
    },
    seccao4: {
        enablingCards: ['seccao5']
    },
    seccao5: {
        enablingCards: []
    },
};
