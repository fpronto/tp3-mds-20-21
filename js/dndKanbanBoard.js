document.addEventListener("dragover", function (event) {
  event.preventDefault();
});

let dragged = null;

const cards = document.getElementsByClassName("custom-card");
for (let i = 0; i < cards.length; i++) {
  const div = cards[i];
  div.addEventListener(
    "dragstart",
    (event) => {
      dragged = event.target;
      event.target.style.opacity = 0.5;
    },
    false
  );
  div.addEventListener(
    "dragend",
    (event) => {
      event.target.style.opacity = "";
    },
    false
  );

  if (i != 0) div.style.display = "none";
}
const columnName = (target) =>
  target.attributes.getNamedItem("column-name").value;

const changeColumnConfiguration = (event) => {
  const toName = columnName(event.target);
  const cardsInColumn = getNumberCards(event);
  switch (toName) {
    case "Reading":
      if (cardsInColumn <= 0) {
        changeLayoutConfigurationColumnUnallowed(event);
      } else {
        changeLayoutConfiguration(event, "reading_counter");
      }
      break;

    case "Read":
      if (cardsInColumn <= 3) {
        changeLayoutConfigurationColumnUnallowed(event);
      } else {
        changeLayoutConfiguration(event, "read_counter");
      }
      break;
    case "Testing":
      if (cardsInColumn <= 1) {
        changeLayoutConfigurationColumnUnallowed(event);
      }
      changeLayoutConfiguration(event, "testing_counter");
      break;
    case "Verify":
      changeLayoutConfigurationColumnUnallowed(event);
      break;
    case "Close":
      changeLayoutConfigurationColumnUnallowed(event);
      break;
    default:
      break;
  }
};

const reverseColumnConfiguration = (event) => {
  const toName = columnName(event.target);
  switch (toName) {
    case "Backlog":
      reverseLayoutConfiguration(event, "backLog_counter");
      break;
    case "Reading":
      reverseLayoutConfiguration(event, "reading_counter");
      break;

    case "Read":
      reverseLayoutConfiguration(event, "read_counter");
      break;
    case "Testing":
      reverseLayoutConfiguration(event, "testing_counter");
      break;
    case "Verify":
      reverseLayoutConfiguration(event, "verify_counter");
      break;
    case "Close":
      reverseLayoutConfiguration(event, "close_counter");
      break;
    default:
      break;
  }
};
const getNumberCards = (event) => {
  const cardsInColumn = event.target.getElementsByClassName("custom-card");
  return cardsInColumn.length;
};

const changeLayoutConfiguration = (event, limitationID) => {
  const limitations = document.getElementById(limitationID);
  event.target.style.border = "dashed Red 2px";
  limitations.style.backgroundColor = "Red";
  limitations.style.color = "White";
};

const changeLayoutConfigurationColumnUnallowed = (event) => {
  event.target.style.border = "dashed Yellow 2px";
};

const reverseLayoutConfiguration = (event, limitationID) => {
  const limitations = document.getElementById(limitationID);
  limitations.style.backgroundColor = "White";
  limitations.style.color = "Black";
  event.target.style.background = "";
  limitations.style.borderColor = "#BDBDBD";
  event.target.style.border = "solid #BDBDBD 2px";
};

const canDrop = (event) => {
  const toName = columnName(event.target);
  const fromName = columnName(dragged.parentNode);
  if (fromName === toName) {
    return false;
  }
  const cardsInColumn = event.target.getElementsByClassName("custom-card");
  let isRead = false;
  let sectionsInLocalStorage = {};
  const movingCardId = dragged.id;

  try {
    sectionsInLocalStorage = JSON.parse(localStorage.getItem("sections"));
  } catch (err) {
    sectionsInLocalStorage = {};
  }
  switch (fromName) {
    case "Backlog":
      try {
        ({ isRead } = sectionsInLocalStorage[movingCardId]);
      } catch (err) { }
      return (
        (toName === "Read" && isRead) ||
        (toName === "Reading" && cardsInColumn.length <= 0) ||
        (toName === "Testing" && cardsInColumn.length <= 0)
      );
    case "Reading":
      return (
        toName === "Backlog" ||
        (toName === "Read" && cardsInColumn.length <= 2) ||
        (toName === "Testing" && cardsInColumn.length <= 0)
      );
    case "Read":
      return (
        toName === "Backlog" ||
        (toName === "Reading" && cardsInColumn.length <= 0) ||
        (toName === "Testing" && cardsInColumn.length <= 0)
      );
    case "Testing":
      try {
        ({ isRead } = sectionsInLocalStorage[movingCardId]);
      } catch (err) { }
      return (
        (toName === "Read" && isRead) ||
        (toName === "Reading" && cardsInColumn.length <= 3) ||
        toName === "Backlog" ||
        toName === "Verify"
      );
    case "Verify":
      try {
        ({ isRead } = sectionsInLocalStorage[movingCardId]);
      } catch (err) { }
      const isCorrect = validateQuestions(movingCardId);
      if (!isCorrect) {
        return (
          toName === "Backlog" ||
          (toName === "Read" && isRead) ||
          (toName === "Reading" && cardsInColumn.length <= 0) ||
          (toName === "Testing" && cardsInColumn.length <= 0)
        );
      } else {
        return toName === "Close";
      }
    case "Close":
      return false;
    default:
      break;
  }
};

const columns = document.getElementsByClassName("custom-column");

const clearReadingContent = () => {
  let readingContentDiv = document.getElementById("reading-content");
  readingContentDiv.innerHTML = "";
  let quizContentDiv = document.getElementById("quiz-content");
  quizContentDiv.innerHTML = "";
};

const renderAllowedCards = (cardId) => {
  cardsHierarchy[cardId].enablingCards.forEach((card) => {
    if (validateQuestions(cardId)) {
      document.getElementById(card).style.display = "flex";
    }
  });
};

for (let i = 0; i < columns.length; i++) {
  const div_column = columns[i];
  div_column.addEventListener(
    "drop",
    (event) => {
      event.preventDefault();
      if (event.target.className === "custom-column") {
        reverseColumnConfiguration(event);
        if (!canDrop(event)) {
          return;
        }
        dragged.style.backgroundColor = "";

        const toName = columnName(event.target);
        const movingCardId = dragged.id;
        clearReadingContent();
        switch (toName) {
          case "Reading":
            let sectionsInLocalStorage = {};
            try {
              sectionsInLocalStorage = JSON.parse(
                localStorage.getItem("sections")
              );
              if (!sectionsInLocalStorage) {
                sectionsInLocalStorage = {};
              }
            } catch (err) { }

            sectionsInLocalStorage[movingCardId] = {
              ...sectionsInLocalStorage[movingCardId],
              isRead: true,
            };

            localStorage.setItem(
              "sections",
              JSON.stringify(sectionsInLocalStorage)
            );
            const read_done = document.createAttribute("read_done");
            read_done.value = "read_done";
            dragged.attributes.setNamedItem(read_done);
            renderSectionDocs(movingCardId);
            break;
          case "Testing":
            renderQuestion(movingCardId);
            break;
          case "Verify":
            const correct = validateQuestions(movingCardId);
            const card = document.getElementById(movingCardId);

            card.style.backgroundColor =
              correct === true ? "#4CAF50" : "#f44336";

            let wrongAnswers = countWrongAnswers.apply(quizzes[movingCardId]);
            score.addWrongs(wrongAnswers);
            break;
          case "Close":
            renderAllowedCards(movingCardId);

            score.closeCard(movingCardId);

            if (score.gameComplete()) {
              const overlay = document.getElementsByClassName(
                "final-overlay-wrapper"
              );
              overlay[0].classList.remove("hide");
              overlay[0].classList.add("show");

              let wrong = document.getElementById("score-wrong-answers");
              let steps = document.getElementById("score-steps");

              wrong.innerText = score.wrongAnswers.toString();
              steps.innerText = score.steps.toString();
            }
            break;
          default:
            break;
        }
        dragged.parentNode.removeChild(dragged);
        event.target.appendChild(dragged);
        updateCounters();
        score.incrementStep();
      }
    },
    false
  );
  div_column.addEventListener(
    "dragenter",
    (event) => {
      if (event.target.className === "custom-column" && canDrop(event)) {
        event.target.style.background = "Lightgrey";
      } else {
        if (event.target.className === "custom-column") {
          changeColumnConfiguration(event);
        }
      }
    },
    false
  );

  div_column.addEventListener(
    "dragleave",
    (event) => {
      if (event.target.className === "custom-column") {
        reverseColumnConfiguration(event);
      }
    },
    false
  );
}

const updateCounters = () => {
  const columns = document.getElementsByClassName("custom-column");
  for (let i = 0; i < columns.length; i++) {
    const div_column = columns[i];
    const counter = div_column.getElementsByClassName(
      "custom-column-header-counter"
    );
    const cards = div_column.getElementsByClassName("custom-card");
    const columnName = div_column.attributes.getNamedItem("column-name").value;
    switch (columnName) {
      case "Backlog":
        counter[0].innerText = `${cards.length}/ -`;
        break;
      case "Read":
        counter[0].innerText = `${cards.length}/ 3`;
        break;
      case "Close":
        counter[0].innerText = `${cards.length}/ -`;
        break;
      case "Reading":
      case "Testing":
        counter[0].innerText = `${cards.length}/1`;
        break;
      default:
        break;
    }
  }
};

updateCounters();
