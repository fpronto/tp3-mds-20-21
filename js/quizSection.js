const seccao1Quiz = {
  id: "seccao1",
  items: [
    {
      question: "Quais os princípios básicos sobre o Kanbam? ",
      answers: [
        "Desenvolvimento Iterativo",
        "Medir o tempo do ciclo",
        "Auto Organização",
        "Visualiza o fluxo trabalho",
      ],
      userChoice: "",
      correct: "d",
    },
  ],
};

const seccao2Quiz = {
  id: "seccao1",
  items: [
    {
      question: "Porque é importante medir o tempo do ciclo:",
      answers: [
        "Otimizar o cycle time",
        "Estimular o desenvolvimento",
        "Pressionar developers",
        "Para saber se o ciclo foi cumprido com sucesso",
      ],
      userChoice: "",
      correct: "a",
    },
  ],
};

const seccao3Quiz = {
  id: "seccao1",
  items: [
    {
      question: "Quais os principais beneficios do Kanban?",
      answers: [
        "Reduz o stock na faixa de 25% -75%, reduzindo assim os custos da empresa.",
        "Organizar o projeto em Sprints traz muito mais foco e organização ",
        "Útil para situações em que as equipes de operações e suporte têm um alto índice de incerteza e variabilidade. ",
      ],
      userChoice: "",
      correct: "a",
    },
    {
      question: "Com kanban podemos fazer uma release",
      answers: [
        "No fim da iteração",
        "Quando pretendermos",
        "Quando é terminada uma user story ",
        "No início da iteração",
      ],
      userChoice: "",
      correct: "b",
    },
  ],
};

const seccao4Quiz = {
  id: "seccao1",
  items: [
    {
      question: "Os cartões devem ser movidos em que sentido?",
      answers: [
        "Horizontal ",
        "Vertical e horizontal ",
        "Vertical ",
        "Apenas da direita para a esquerda ",
      ],
      userChoice: "",
      correct: "b",
    },
    {
      question:
        "Posso adicionar tarefas no quadro kanban em qualquer fase do projeto?",
      answers: [
        "Não, só posso adicionar tarefas na fase de análise de requisitos",
        "Não só posso adicionar tarefas nas fases de análise de requisitos e design da aplicação",
        "Sim",
        "Não sei",
      ],
      userChoice: "",
      correct: "c",
    },
  ],
};

const seccao5Quiz = {
  id: "seccao1",
  items: [
    {
      question:
        "Qual das seguintes abordagens é a melhor para projetos em que o cliente pretende receber releases sempre que for concluída uma user story?",
      answers: ["Scrum", "Waterfall", "Agile", "Kanban"],
      userChoice: "",
      correct: "d",
    },
    {
      question:
        "Qual a melhor abordagem para projetos que tem grande probabilidade de introduzir novas stories a meio do desenvolvimento?",
      answers: ["Scrum", "Kanban", "Waterfall", "Agile"],
      userChoice: "",
      correct: "b",
    },
  ],
};

const answerOptions = ["a", "b", "c", "d"];

function countWrongAnswers() {
  return this.items.filter((i) => i.correct !== i.userChoice).length;
};

let quizzes = {
  seccao1: seccao1Quiz,
  seccao2: seccao2Quiz,
  seccao3: seccao3Quiz,
  seccao4: seccao4Quiz,
  seccao5: seccao5Quiz,
};

let quizContentDiv = document.getElementById("quiz-content");

function getCorrectAnwser(id) {
  let seccaoQuiz = quizzes[id];
  if (!seccaoQuiz) {
    return;
  }
  return seccaoQuiz.items.map((question) => question.correct);
}

function getUserChoiceAnwser(id) {
  let seccaoQuiz = quizzes[id];
  if (!seccaoQuiz) {
    return;
  }
  return seccaoQuiz.items.map((question) => question.userChoice);
}

const validateQuestions = (id) => {
  const correctAnwsers = getCorrectAnwser(id);
  const userChoices = getUserChoiceAnwser(id);
  const correct = correctAnwsers
    .map((correctAnwser, i) => correctAnwser === userChoices[i])
    .every(Boolean);
  return correct;
};

function renderQuestion(_id) {
  let seccaoQuiz = quizzes[_id];
  if (!seccaoQuiz) {
    return;
  }
  seccaoQuiz.items.forEach((quizItem, i) => {
    let questionDiv = document.createElement("h2");
    questionDiv.append(quizItem.question);
    quizContentDiv.append(questionDiv);
    let div = document.createElement("div");
    div.classList.add("wrapper");

    quizItem.answers.forEach((optionText, quizItemIndex) => {
      let optionRadio = document.createElement("input");
      optionRadio.setAttribute("type", "radio");
      optionRadio.addEventListener("change", (event) => {
        quizItem.userChoice = event.target.value;
      });

      optionRadio.name = `answer${i}`;
      optionRadio.value = answerOptions[quizItemIndex];
      optionRadio.id = `${answerOptions[quizItemIndex]}-question${i}`;
      if (quizItem.userChoice) {
        optionRadio.checked =
          answerOptions[quizItemIndex] === quizItem.userChoice;
      }

      let newlabel = document.createElement("Label");
      newlabel.setAttribute("for", optionRadio.id);
      newlabel.innerHTML = optionText;
      let div2 = document.createElement("div"); 
      div2.classList.add("div2");
      div2.appendChild(optionRadio);
      div2.appendChild(newlabel);
      div.appendChild(div2);
      
    });
    quizContentDiv.appendChild(div);
    quizContentDiv.appendChild(document.createElement("br"));
  });
}
