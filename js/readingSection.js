const seccao1 = {
  isRead: false,
  content: [
    {
      title: "O que é o Kanban?",
      text:
        "Kanban é um método de gestão para equipas e organizações visualizarem \
        o seu trabalho, identificar e eliminar bottlenecks e alcançar melhorias \
        operacionais dramáticas em termos de rendimento e qualidade. \n \
        Kanban é um método para melhorar gradualmente tudo o que fazemos, \
        seja desenvolvimento de software, TI / Ops, Pessoal, Recrutamento, \
        Marketing e Vendas ou Aquisições. Na verdade, quase todas as funções \
        de negócios podem se beneficiar da aplicação de Kanban para trazer \
        benefícios significativos, como lead time reduzido, maior rendimento \
        e qualidade muito maior de produtos ou serviços entregues.",
    },
  ],
};
const seccao2 = {
  isRead: false,
  content: [
    {
      title: "Princípios Básicos",
      text:
        "Visualiza o fluxo trabalho -> Divide o trabalho em parte,\
        escreve cada item num cartão e coloca na parede. Usa colunas \
        para ilustrar onde cada item pertence ao fluxo de trabalho.\n\
        Limita o trabalho em andamento (WIP) -> Atribui limites explícitos \
        para quantos itens podem estar em “progresso”.\n\
        Medir o tempo de espera -> Tempo médio para concluir um item desde a solicitação até a entrega.\n\
        Medir o tempo do ciclo -> Tempo desde o início do trabalho até a \
        preparação da entrega. Otimizar o processo para tornar o \
        lead time/cycle time tão pequeno e previsível quanto possível.",
    },
  ],
};
const seccao3 = {
  isRead: false,
  content: [
    {
      title: "Benefícios Kanban",
      text:
        "Os Bottlenecks tornam-se claramente visíveis em tempo real.\n\
        Reduz o stock na faixa de 25% -75%, reduzindo assim os custos da empresa.\n\
        Como todos os segmentos / estados do workflow são organizados \
        visualmente, os itens necessários, reduzindo os tempos de espera e \
        garantindo a velocidade, suportam continuamente todas as tarefas do \
        workflow, útil para situações em que as equipes de operações e suporte \
        têm um alto índice de incerteza e variabilidade.\n\
        Uma release pode ser feita em qualquer fase do projeto. \
        É um desafio configurar o processo de desenvolvimento desta forma. \
        É necessário ter uma política de controlo de “branch by feature”, unir, integrar e testar frequentemente. ",
    },
  ],
};
const seccao4 = {
  isRead: false,
  content: [
    {
      title: "Caracteristicas da Kanban board",
      text:
        "Um quadro Kanban é uma das ferramentas que podem ser usadas \
        para implementar Kanban. O kanban pode ser usado para organizar \
        várias áreas de uma organização e pode ser projetado de acordo com esta.\n\
        Os quadros Kanban representam visualmente o trabalho em várias \
        fases, usando cartões para representar itens de trabalho e colunas \
        para representar cada fase do processo. Os cartões são movidos na \
        horizontal para mostrar o progresso e ajudar a coordenar as equipas \
        que executam o trabalho, estes podem ainda ser movidos na \
        vertical para definir a importância do cartão. ",
    },
  ],
};
const seccao5 = {
  isRead: false,
  content: [
    {
      title: "Projetos que se enquadrão em Kanban",
      text:
        "Quando uma importante user story fica implementada, \
        podemos fazer a release para o cliente. O cliente beneficia \
        com realeases mais frequentes e também faz aumentar a \
        sua satisfação. Para tudo isto funcionar, é necessário ter um \
        conjunto de testes automatizados, caso contrário, pode ser \
        problemático fazer pequenas releases com boa qualidade. \n\
        No Scrum, não há a possibilidade de adicionar stories com \
        facilidade e rapidez durante uma sprint. Pelo menos não é \
        um processo fácil e a equipa de desenvolvimento normalmente \
        resiste em substituir uma story no Sprint backlog. Uma nova \
        story pode ser discutida à pressa, alguns detalhes podem ser \
        esquecidos e, como resultado, será necessário refazer parte do trabalho.\n\
        No kanban se houver um requisito urgente para ser implementado \
        ou uma importante user story, podemos simplesmente colocá-la no topo da fila.",
    },
  ],
};

const readings = { seccao1, seccao2, seccao3, seccao4, seccao5 };

const readingContentDiv = document.getElementById("reading-content");

function renderSectionDocs(id) {
  const reading = readings[id];
  if (reading) {
    reading.content.forEach(function (item) {
      const titleDiv = document.createElement("h2");
      const textDiv = document.createElement("div");
      titleDiv.classList.add("titulo_conteudo");
      textDiv.classList.add("texto_conteudo");

      titleDiv.append(item.title);
      textDiv.append(item.text);

      readingContentDiv.append(titleDiv);
      readingContentDiv.append(textDiv);
    });
  }
}
